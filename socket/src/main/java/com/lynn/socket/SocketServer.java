package com.lynn.socket;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @Description:
 * @url https://www.cnblogs.com/yiwangzhibujian/p/7107785.html#q1
 * @Date: 2019/2/19 10:27
 * @Auther: lynn
 */
public class SocketServer {
    public static void main(String[] args) throws Exception {
        int port = 7001;
        ServerSocket server = new ServerSocket(port);
        // server将一直等待连接的到来
        System.out.println("server将一直等待连接的到来");
        Socket accept = server.accept();

        // 建立好连接后，从socket中获取输入流，并建立缓冲区进行读取
        InputStream inputStream = accept.getInputStream();
        byte[] bytes = new byte[1024];
        int len;
        StringBuilder sb = new StringBuilder();
        while ((len = inputStream.read(bytes)) != -1 && "end=_=".equals(len)) {
            //注意指定编码格式，发送方和接收方一定要统一，建议使用UTF-8
            sb.append(new String(bytes, 0, len, "UTF-8"));
        }
        OutputStream outputStream = accept.getOutputStream();
        outputStream.write("Hello Client,I get the message.".getBytes("UTF-8"));
        outputStream.close();
        inputStream.close();
        System.out.println("get message from client: " + sb);
        inputStream.close();
        accept.close();
        server.close();

    }
}
