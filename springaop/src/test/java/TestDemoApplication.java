import com.aop.DemoApplication;
import com.aop.bean.Product;
import com.aop.log.LogService;
import com.aop.security.CurrentUserHolder;
import com.aop.service.ProductService;
import com.aop.service.sub.SubService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class)
public class TestDemoApplication {
    @Autowired
    ProductService productService;
    @Autowired
    SubService subService;
    @Autowired
    LogService logService;

    @Test
    public void testInsert() {
        int[] p = new int[]{};
        CurrentUserHolder.set("tom");
        productService.insertProduct(new Product(1L, "面包"));
    }

    @Test
    public void testDelete() {
        CurrentUserHolder.set("admin");
        productService.deleteProduct(1L);
    }

    @Test
    public void testWeiyi() {
        System.out.println(8 << 1);
        System.out.println(9 >> 1);
    }

    @Test
    public void testConfig() {
        System.out.println("###begin test###");
        productService.findById(1L);
        productService.findByTwoArgs(1L, "hello");
        productService.getName();
        subService.demo();
        try {
            productService.exDemo();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        logService.log();
        productService.log();
        logService.annoArg(new Product());
    }
}
