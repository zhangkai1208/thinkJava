package com.aop.service;

import com.aop.security.CurrentUserHolder;
import org.springframework.stereotype.Service;

@Service
public class AuthService {
    public void checkAuth() {
        if (!CurrentUserHolder.get().equals("admin")) {
            throw new RuntimeException("operation not allow!");
        }
    }
}
