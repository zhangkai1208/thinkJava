package com.aop.service;

import com.aop.anno.AdminOnly;
import com.aop.bean.Product;
import com.aop.log.Loggable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProductService implements Loggable {
    @Autowired
    private AuthService authService;

    @AdminOnly//now
    public void deleteProduct(long id) {
        //传统authService.checkAuth();
        System.out.println("delete product! where id = " + id);
    }

    @AdminOnly//now
    public void insertProduct(Product product) {
        //传统authService.checkAuth();
        System.out.println("insert product! product is [" + product + "]");
    }

    public String getName() {
        System.out.println("execute get name");
        return "product service";
    }

    public void exDemo() throws IllegalAccessException {
        System.out.println("execute ex demo");
        throw new IllegalAccessException("TEST");
    }

    public void findById(Long id) {
        System.out.println("execute find by id");
    }

    public void findByTwoArgs(Long id, String name) {
        System.out.println("execute find by id and name");
    }

    @Override
    public void log() {
        System.out.println("log from product service");
    }
}
