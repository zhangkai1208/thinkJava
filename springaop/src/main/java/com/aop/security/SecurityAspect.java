package com.aop.security;

import com.aop.service.AuthService;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class SecurityAspect {
//    @Autowired
//    AuthService authService;
//
//    @Pointcut("@annotation(com.aop.anno.AdminOnly)")
//    public void adminOnly() {
//        System.out.println("-----adminOnly-----");
//    }
//
//    @Before("adminOnly()")
//    public void check() {
//        authService.checkAuth();
//        System.out.println("-----checkSuccess-----");
//    }
}
