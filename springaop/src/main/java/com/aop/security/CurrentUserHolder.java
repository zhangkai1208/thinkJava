package com.aop.security;

public class CurrentUserHolder {
    private static final ThreadLocal<String> local = new ThreadLocal<>();

    public static String get() {
        return null == local.get() ? "unknown" : local.get();
    }

    public static void set(String user) {
        local.set(user);
    }
}
