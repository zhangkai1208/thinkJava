package com.aop.log;

/**
 * Created by cat on 2017-02-19.
 */
public interface Loggable {
     void log();
}
