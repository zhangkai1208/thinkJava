package com.aop.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface AdminOnly {
}
// Initialization of bean failed; nested exception is java.lang.IllegalArgumentException: error Type referred to is not an annotation type: AdminOnly
