package com.chain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

public class ChainClient {
    public static void main(String[] args) {
        Chain chain = new Chain(Arrays.asList(new HandlerD(), new HandlerE(), new HandlerF()));
        chain.process();
    }
}

class HandlerD extends ChainHandler {

    @Override
    void handleProcess() {
        System.out.println("handler d");
    }
}

class HandlerE extends ChainHandler {

    @Override
    void handleProcess() {
        System.out.println("handler e");
    }
}

class HandlerF extends ChainHandler {

    @Override
    void handleProcess() {
        System.out.println("handler f");
    }
}
