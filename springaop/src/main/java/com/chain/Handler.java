package com.chain;

import lombok.Data;

@Data
public abstract class Handler {
    private Handler sucessor;

    public Handler() {
    }

    public Handler(Handler sucessor) {
        this.sucessor = sucessor;
    }

    void execute() {
        handleProcess();
        if (null != sucessor) {
            sucessor.execute();
        }
    }

    abstract void handleProcess();
}
