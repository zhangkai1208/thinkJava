package com.chain;

public class Client {
    public static void main(String[] args) {
        Handler handlerA = new HandlerA();
        Handler handlerB = new HandlerB();
        Handler handlerC = new HandlerC();
        //设置责任链关系
        handlerA.setSucessor(handlerB);
        handlerB.setSucessor(handlerC);

        handlerA.execute();

    }
}

class HandlerA extends Handler {

    @Override
    void handleProcess() {
        System.out.println("handler a");
    }
}

class HandlerB extends Handler {

    @Override
    void handleProcess() {
        System.out.println("handler b");
    }
}

class HandlerC extends Handler {

    @Override
    void handleProcess() {
        System.out.println("handler c");
    }
}
