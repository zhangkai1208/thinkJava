package com.chain;

import lombok.*;

import java.util.List;

public abstract class ChainHandler {

    void execute(Chain chain) {
        handleProcess();
        if (null == chain) {
            throw new NullPointerException("Chain is null!");
        }
        chain.process();
    }

    abstract void handleProcess();
}

@NoArgsConstructor
@RequiredArgsConstructor
class Chain {
    @NonNull
    List<ChainHandler> handlerList;
    int index = 0;

    public void process() {
        if (index >= handlerList.size()) {
            return;
        }
        handlerList.get(index++).execute(this);
    }
}
