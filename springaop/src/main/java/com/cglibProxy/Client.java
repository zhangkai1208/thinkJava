package com.cglibProxy;

import net.sf.cglib.proxy.Enhancer;

public class Client {
    public static void main(String[] args) {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(SubProject.class);
        enhancer.setCallback(new CglibProxy());
        SubProject subProject = (SubProject) enhancer.create();
        subProject.hello();
    }
}
