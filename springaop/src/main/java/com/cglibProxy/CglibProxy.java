package com.cglibProxy;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

public class CglibProxy implements MethodInterceptor {

    @Override
    public Object intercept(Object o, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
        Object result = null;
        try {
            System.out.println("before");
            result = methodProxy.invokeSuper(o, args);
        } catch (Exception e) {
            System.out.println("get ex:" + e.getMessage());
        } finally {
            System.out.println("after");
        }
        return result;
    }
}

interface Project1 {
    void hello();
}

class SubProject implements Project1 {

    @Override
    public void hello() {
        System.out.println("hello");
    }
}
