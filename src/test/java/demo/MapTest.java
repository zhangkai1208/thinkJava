package demo;

import org.junit.jupiter.api.Test;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;

public class MapTest {
    @Test
    public void testMap() {
        HashMap<Integer, String> integerStringHashMap = new HashMap<>();
        integerStringHashMap.put(1, "zhangkai");
        integerStringHashMap.put(2, "wq");
        integerStringHashMap.put(3, "kj");
        HashMap<Integer, String> integerStringHashMap1 = new HashMap<>(integerStringHashMap);
        System.out.println(integerStringHashMap);
        System.out.println(integerStringHashMap1);
        integerStringHashMap.put(3, "kjj");
        System.out.println(integerStringHashMap);
        System.out.println(integerStringHashMap1);
        integerStringHashMap1.put(3, "kjjjjjj");
        System.out.println(integerStringHashMap);
        System.out.println(integerStringHashMap1);
        integerStringHashMap.remove(3);
        System.out.println(integerStringHashMap);
        System.out.println(integerStringHashMap1);
        System.out.println(integerStringHashMap.hashCode());
        System.out.println(integerStringHashMap1.hashCode());
        System.out.println(integerStringHashMap.getOrDefault(8, "zhangkai"));
        System.out.println(integerStringHashMap.putIfAbsent(8, "zhangkai"));
        System.out.println(integerStringHashMap);
    }
}
