package demo;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

public class CollectionTest {
    @Test
    public void collection() {
        Collection<String> collection = new HashSet<>();
        collection.add("1");
        collection.add("2");
        System.out.println(collection);
        System.out.println(collection.toArray() instanceof Object[]);
    }
    //若ArrayList中要添加大量元素，则使用ensureCapacity(int n)方法一次性增加，可优化运行效率
    @Test
    public void arrayList() {
        ArrayList<Object> list = new ArrayList<>();
        final int N =10000000;
        long startTime = System.currentTimeMillis();
        for(int i = 0;i<N;i++) {
            list.add(i);
        }
        long endTime = System.currentTimeMillis();
        System.out.println(endTime-startTime);

        list = new ArrayList<>();
        long startTime1 = System.currentTimeMillis();
        list.ensureCapacity(N);
        for(int i = 0;i<N;i++) {
            list.add(i);
        }
        long endTime1 = System.currentTimeMillis();
        System.out.println(endTime1-startTime1);
    }
}
