package demo;

import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.nio.Buffer;
import java.nio.ByteBuffer;

public class StringBuilderTest {
    @Test
    public void testStringBuild() {
        StringBuilder stringBuilder = new StringBuilder("zhangk");
        stringBuilder.appendCodePoint(97);
        stringBuilder.append('i');
        System.out.println(stringBuilder.toString());
        char a[] = {97};
        stringBuilder.insert(4, a);
        System.out.println(stringBuilder.toString());
    }

    @Test
    void testBuffer() {
        ByteBuffer byteBuffer = ByteBuffer.allocate(10);
        byteBuffer.put(new Byte("1"));
        byteBuffer.put(new Byte("2"));
        byteBuffer.put(new Byte("2"));
        byteBuffer.put(new Byte("2"));
        byteBuffer.put(new Byte("2"));
        byteBuffer.put(new Byte("2"));
        byteBuffer.put(new Byte("2"));
        byteBuffer.put(new Byte("2"));
        byteBuffer.put(new Byte("2"));
        Byte bt = 0x52;
        byteBuffer.put(bt);
        byte b = byteBuffer.get(8);
        System.out.println(byteBuffer);
        System.out.println(b);
    }
}
