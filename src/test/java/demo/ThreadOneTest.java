package demo;

import org.junit.jupiter.api.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadOneTest {
    @Test
    public void test01() {
        ExecutorService service = Executors.newCachedThreadPool(); // 创建一个线程池
        for (int i = 0; i < 100; i++) {
            Runnable runnable = () -> {
                try {
                    System.out.println(Thread.currentThread().getName() + ":" + DateUtil.parse("2017-06-24 06:02:20"));
                    Thread.sleep(30000);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            };
            service.execute(runnable);// 为线程池添加任务
        }
    }
}

class DateUtil {
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static String formatDate(Date date) throws ParseException {
        synchronized (sdf) {
            return sdf.format(date);
        }
    }

    public static Date parse(String strDate) throws ParseException {
        synchronized (sdf) {
            return sdf.parse(strDate);
        }
    }
}
class ConcurrentDateUtil {

    private static ThreadLocal<DateFormat> threadLocal = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        }
    };

    public static Date parse(String dateStr) throws ParseException {
        return threadLocal.get().parse(dateStr);
    }

    public static String format(Date date) {
        return threadLocal.get().format(date);
    }
}
