package design.proxy;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;
import java.util.ArrayList;

public class ProxyCglib {
    public static void main(String[] args) {
        Intermediary intermediary = new Intermediary();

        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(Landlord.class);
        enhancer.setCallback(intermediary);

        Landlord rentProxy = (Landlord) enhancer.create();
        rentProxy.rent();
        new ArrayList<>().remove(1);
    }
}

class Landlord {
    public void rent() {
        System.out.println("房东要出租房子了！");
    }
}

class Intermediary implements MethodInterceptor {

    @Override
    public Object intercept(Object object, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
        Object intercept = methodProxy.invokeSuper(object, args);
        System.out.println("中介：该房源已发布！");
        return intercept;
    }
}
