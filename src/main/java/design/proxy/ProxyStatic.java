package design.proxy;

/**
 * 静态代理
 * 因为代理对象需要与目标对象实现一样的接口,所以会有很多代理类,类太多.同时,一旦接口增加方法,目标对象与代理对象都要维护.
 * 如何解决静态代理中的缺点呢?答案是可以使用动态代理方式
 */
public class ProxyStatic {
    public static void main(String[] args) {
        ProxyFactory proxy = new ProxyFactory(new UserDaoImpl());
        proxy.save();
    }
}

interface IUserDao {
    void save();
}

class UserDaoImpl implements IUserDao {

    private IUserDao userDao;

    @Override
    public void save() {
        System.out.println("保存数据成功!");
    }
}

class ProxyFactory implements IUserDao {
    private IUserDao userDao;

    public ProxyFactory(IUserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public void save() {
        System.out.println("Transition begin!");
        userDao.save();
        System.out.println("Transition end!");
    }
}
