package design.proxy;

import java.lang.reflect.Proxy;

/**
 * JDK代理 也是动态代理
 */
public class ProxyJdk {
    public static void main(String[] args) {
        Rent rent = new Landlord1();
        Rent rentProxy = (Rent) Proxy.newProxyInstance(rent.getClass().getClassLoader(), rent.getClass().getInterfaces(), (proxy, method, args1) -> {
            Object invoke = method.invoke(rent, args1);
            System.out.println("中介：该房源已发布！");
            return invoke;
        });
        rentProxy.rent();
        System.out.println(rent);
    }

}

class Landlord1 implements Rent {
    private String name = "房东名";
    @Override
    public void rent() {
        System.out.println("房东要出租房子了！");
    }

    @Override
    public String toString() {
        return "Landlord1{" +
                "name='" + name + '\'' +
                '}';
    }
}
//
//class Intermediary1 implements InvocationHandler {
//
//    private Object post;
//
//    Intermediary1(Object post) {
//        this.post = post;
//    }
//
//    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
//        Object invoke = method.invoke(post, args);
//        System.out.println("中介：该房源已发布！");
//        return invoke;
//    }
//}

interface Rent {
    void rent();
}



