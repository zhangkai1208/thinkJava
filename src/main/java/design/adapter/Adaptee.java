package design.adapter;

public class Adaptee {
    //原有的业务逻辑
    public void volt220() {
        System.out.println("220V电压");
    }
}

interface Target {
    void volt5();
}

class ConcreteTarget implements Target {
    @Override
    public void volt5() {
        System.out.println("5V电压");
    }
}
