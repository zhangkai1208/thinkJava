package design.decorate;

public abstract class Component {
    abstract void operate();
}

class ConcreateComponet extends Component {

    @Override
    void operate() {
        System.out.println("operate");
    }
}

abstract class Decorator extends Component {
    private Component component;

    Decorator(Component component) {
        this.component = component;
    }

    @Override
    void operate() {
        //委托给被修饰者去执行对应的方法
        this.component.operate();
    }
}

class ConcreateDecorator1 extends Decorator {

    ConcreateDecorator1(Component component) {
        super(component);
    }

    @Override
    void operate() {
        operate1();
        super.operate();
    }

    void operate1() {
        System.out.println("修饰方法1");
    }

}

class ConcreateDecorator2 extends Decorator {

    ConcreateDecorator2(Component component) {
        super(component);
    }

    @Override
    void operate() {
        operate2();
        super.operate();
    }

    void operate2() {
        System.out.println("修饰方法2");
    }

}
