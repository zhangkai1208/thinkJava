package design.decorate;

public class Client {
    public static void main(String[] args) {
        Component component = new ConcreateDecorator1(new ConcreateDecorator2(new ConcreateComponet()));
        component.operate();
    }
}
