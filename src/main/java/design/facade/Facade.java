package design.facade;

/**
 * 门面模式，也叫外观模式，是一种比较常用的封装模式：要求一个子系统的外部与其内部的通信必须通过一个统一的对象进行。
 *          门面模式提供一个高层次的接口，使得子系统更易于使用。
 */
public class Facade {
    private FacadeA fa = new FacadeA();
    private FacadeB fb = new FacadeB();
    private FacadeC fc = new FacadeC();

    public void doSomthing(){
        fa.doSomthing();
        fb.doSomthing();
        fc.doSomthing();
    }

    public static void main(String[] args) {
        Facade facade = new Facade();
        facade.doSomthing();
    }
}
class FacadeA{
    void doSomthing(){
        System.out.println("FacadeA doSomthing");
    }
}
class FacadeB{
    void doSomthing(){
        System.out.println("FacadeB doSomthing");
    }
}
class FacadeC{
    void doSomthing(){
        System.out.println("FacadeC doSomthing");
    }
}
