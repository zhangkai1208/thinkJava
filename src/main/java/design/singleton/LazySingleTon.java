package design.singleton;

/**
 * 懒汉式
 * 特点：Lazy初始化；线程安全，但是由于每次需要同步性能较低，不建议使用
 * 学习网址https://mp.weixin.qq.com/s/jtA0FYbaTBSzSO9NcczdYw
 */
class LazySingleTon {
    private static LazySingleTon lazySingleTon;

    private LazySingleTon() {

    }

    public static synchronized LazySingleTon getSingleTon() {
        if (lazySingleTon == null) {
            lazySingleTon = new LazySingleTon();
        }
        return lazySingleTon;
    }

    public static void main(String[] args) {
//        if (LazySingleTon.getSingleTon().equals(LazySingleTon.getSingleTon())) {
//        if (LazySingleTon.getSingleTon() == LazySingleTon.getSingleTon() ) {
        if (LazySingleTon.getSingleTon().hashCode() == LazySingleTon.getSingleTon().hashCode()) {//三种方式都是比较对象的地址层次递进
            System.out.println("SingleTonOne == SingleTonTwo");
        } else {
            System.out.println("SingleTonOne != SingleTonTwo");
        }
    }
}


