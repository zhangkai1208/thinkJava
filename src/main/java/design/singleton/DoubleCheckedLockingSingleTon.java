package design.singleton;

/**
 * 双检锁/双重校验锁（DCL，即 double-checked locking）
 * 特点：懒汉式的改进版,Lazy初始化;线程安全，且在多线程情况下能保持高性能
 */
public class DoubleCheckedLockingSingleTon {
    private static DoubleCheckedLockingSingleTon singleTon;

    private DoubleCheckedLockingSingleTon() {

    }

    public static DoubleCheckedLockingSingleTon getSingleTon() {
        if (singleTon == null) {
            synchronized (DoubleCheckedLockingSingleTon.class) {
                if (singleTon == null) {
                    singleTon = new DoubleCheckedLockingSingleTon();
                }
            }
        }
        return singleTon;
    }

    public static void main(String[] args) {
        if (DoubleCheckedLockingSingleTon.getSingleTon().hashCode() == DoubleCheckedLockingSingleTon.getSingleTon().hashCode()) {//三种方式都是比较对象的地址层次递进
            System.out.println("DoubleCheckedLockingSingleTonOne == DoubleCheckedLockingSingleTonTwo");
        } else {
            System.out.println("DoubleCheckedLockingSingleTonOne != DoubleCheckedLockingSingleTonTwo");
        }
    }
}
