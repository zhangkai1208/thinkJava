package design.singleton;

/**
 * 饿汉式
 * 特点：非Lazy初始化，浪费内存；线程安全，基于ClassLoader机制避免了多线程的同步问题
 */
public class StarveSingleTon {
    //方式一：类装载的时候初始化
    private static StarveSingleTon singleTon = new StarveSingleTon();

    //方式二：类初始化的时候才去初始化
//    static {
//        singleTon = new StarveSingleTon();
//    }

    private StarveSingleTon() {

    }

    public static synchronized StarveSingleTon getSingleTon() {
        return singleTon;
    }
}
