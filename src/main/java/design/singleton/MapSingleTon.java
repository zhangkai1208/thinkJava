package design.singleton;

import java.util.HashMap;
import java.util.Map;

public class MapSingleTon {
    public static class SingletonManager {

        private SingletonManager() {

        }

        private static Map<String, MapSingleTon> sSingletonMap = new HashMap<>();

        static void register(String key, MapSingleTon value) {
            if (!sSingletonMap.containsKey(key)) {
                sSingletonMap.put(key, value);
            }
        }

        public static void unregister(String key) {
            if (sSingletonMap.containsKey(key)) {
                sSingletonMap.remove(key);
            }
        }


        static MapSingleTon getSingleton(String key) {
            return sSingletonMap.get(key);
        }
    }

    public static void main(String[] args) {
        MapSingleTon singleTon = new MapSingleTon();
        int hashOne = singleTon.hashCode();
        MapSingleTon.SingletonManager.register("demo", singleTon);
        int hash = MapSingleTon.SingletonManager.getSingleton("demo").hashCode();
        System.out.println(hash == hashOne);
    }
}
