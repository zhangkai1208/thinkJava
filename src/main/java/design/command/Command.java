package design.command;

public abstract class Command {

    protected Receiver mReceiver;

    Command(Receiver mReceiver) {
        this.mReceiver = mReceiver;
    }

    public abstract void execute();


    public static void main(String[] args) {
            Invoker invoker = new Invoker();
            invoker.setCommand(new ProgrommerCommand());
            invoker.action();

            invoker.setCommand(new DesiginerCommand());
            invoker.action();

    }

}

class ProgrommerCommand extends Command {
     ProgrommerCommand() {
        super(new Progrommer());
    }

    public ProgrommerCommand(Receiver mReceiver) {
        super(mReceiver);
    }

    @Override
    public void execute() {
        this.mReceiver.doSomthing();
    }
}

class DesiginerCommand extends Command {
     DesiginerCommand() {
        super(new Desiginer());
    }

    public DesiginerCommand(Receiver mReceiver) {
        super(mReceiver);
    }

    @Override
    public void execute() {
        this.mReceiver.doSomthing();
    }
}

class Invoker {
    private Command command;

    public void setCommand(Command command) {
        this.command = command;
    }

    public void action() {
        command.execute();
    }
}

abstract class Receiver {
    abstract void doSomthing();
}

//程序员也是接收者
class Progrommer extends Receiver {

    @Override
    void doSomthing() {
        System.out.println("程序员写代码!");
    }
}

//设计师也是接收者
class Desiginer extends Receiver {

    @Override
    void doSomthing() {
        System.out.println("UI设计师画画!");
    }
}

