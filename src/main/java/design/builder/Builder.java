package design.builder;

public class Builder {
}

class Product {
    private String mPartA;
    private String mPartB;
    private String mPartC;

    private static class Builder {
        Product mProduct;

        Builder() {
            this.mProduct = new Product();
        }

        Builder builderA(String mPartA) {
            mProduct.mPartA = mPartA;
            return this;
        }

        Builder builderB(String mPartB) {
            mProduct.mPartB = mPartB;
            return this;
        }

        Builder builderC(String mPartC) {
            mProduct.mPartC = mPartC;
            return this;
        }

        Product build() {
            return mProduct;
        }
    }

    public static void main(String[] args) {
        Product product = new Product.Builder().builderA("PartA").builderB("PartB").builderC("PartC").build();
        System.out.println(product.mPartA);
        System.out.println(product.mPartB);
        System.out.println(product.mPartC);
    }
}
