package design.factory;

public class Games {
    public static void main(String[] args) {
        playGame(new CheckerFactory());
        playGame(new ChessFactory());
    }

    static void playGame(GameFactory factory) {
        Game game = factory.getGame();
        while (game.move()) ;
    }
}

interface Game {
    boolean move();
}

interface GameFactory {
    Game getGame();
}

class Checkers implements Game {
    private int moves = 0;
    static final int MOVES = 3;

    public boolean move() {
        System.out.println("Checkers move " + moves);
        return ++moves != MOVES;
    }
}

class Chess implements Game {

    private int moves = 0;
    static final int MOVES = 3;

    public boolean move() {
        System.out.println("Chess move " + moves);
        return ++moves != MOVES;
    }
}

class CheckerFactory implements GameFactory {

    public Game getGame() {
        return new Checkers();
    }
}

class ChessFactory implements GameFactory {

    public Game getGame() {
        return new Chess();
    }
}
