package design.factory.abstractFactory;

public abstract class Factory {
    public abstract ProductA createProductA();

    public abstract ProductB createProductB();
}
