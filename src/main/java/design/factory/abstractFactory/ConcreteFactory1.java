package design.factory.abstractFactory;

public class ConcreteFactory1 extends Factory {
    @Override
    public ConcreateProductA1 createProductA() {
        return new ConcreateProductA1();
    }

    @Override
    public ConcreateProductB1 createProductB() {
        return new ConcreateProductB1();
    }
}

class ConcreteFactory2 extends Factory {
    @Override
    public ConcreateProductA2 createProductA() {
        return new ConcreateProductA2();
    }

    @Override
    public ConcreateProductB2 createProductB() {
        return new ConcreateProductB2();
    }
}


class ConcreateProductA1 extends ProductA {
}

class ConcreateProductB1 extends ProductB {
}

class ConcreateProductA2 extends ProductA {
}

class ConcreateProductB2 extends ProductB {
}
