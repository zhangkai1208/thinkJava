package design.factory.abstractFactory;

abstract class ProductB {
}

class Product1 extends ProductB {

}

class Product3 extends ProductA {

}

class Product2 extends ProductB {

}

class Product14 extends ProductA {

}

abstract class ProductA {

}
