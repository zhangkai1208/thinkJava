package design.factory.simple;

abstract class Factory {
    abstract <T extends Product> T create(Class<T> clz);
}
