package design.factory.simple;

abstract class Product {
    abstract void sell();
}

class FastFood extends Product {

    @Override
    void sell() {
        System.out.println("快餐出售");
    }
}

class HealthProduct extends Product {

    @Override
    void sell() {
        System.out.println("保健品出售");
    }
}
