package design.factory.simple;
/**https://mp.weixin.qq.com/s?__biz=MzIwNDU0MDg3Ng==&mid=2247483979&idx=1&sn=f08f551153174bffac2d96f7c31e0478&chksm=973fdce8a04855fe134b07124b08a7ed2dbca052885961c5201631b9e754473b1d97a9aada13&scene=0#rd
 * */
public class Client {
    public static void main(String[] args) {
        ConcreateFactory factory = new ConcreateFactory();
        factory.create(FastFood.class).sell();
        factory.create(HealthProduct.class).sell();
    }
}
