package design.factory.simple;

public class ConcreateFactory extends Factory {
    @Override
     <T extends Product> T create(Class<T> clz) {
        Product product = null;
        try {
            product = (Product) Class.forName(clz.getName()).newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (T) product;
    }
}
