package com.lynn.typeinfo;

import java.util.Arrays;
import java.util.List;

public class TypeInfo {
    public static final String aClass = "TypeInfo";
    public static void main(String[] args) {
        Circle circle = new Circle();
        System.out.println(circle + "");
//        List<Shape> list = new ArrayList<Shape>();
//        list.add(circle);
//        System.out.println(list.get(0));
//        list.get(0).draw();
        List<Shape> lists = Arrays.asList((Shape) new Circle());
        for (Shape shape : lists) {
            System.out.println(shape instanceof Circle);
            System.out.println(shape instanceof Shape);
            shape.draw();
        }

    }
}

abstract class Shape {
    void draw() {
    }

    public abstract String toString();
}

class Circle extends Shape {
    void draw() {
        System.out.println("Circle draw");
    }

    public String toString() {
        return "Circle";
    }
}
