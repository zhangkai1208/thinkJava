package com.lynn.typeinfo;

public class Claze {
    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        System.out.println();
        Class<TypeInfo> aClass = (Class<TypeInfo>) Class.forName("com.lynn.typeinfo.TypeInfo");
        TypeInfo typeInfo = aClass.newInstance();
        System.out.println(typeInfo.aClass);
        System.out.println(Boolean.TYPE == boolean.class);
        System.out.println(Character.TYPE == char.class);
        System.out.println(Byte.TYPE == byte.class);
        System.out.println(Short.TYPE == short.class);
        System.out.println(Integer.TYPE == int.class);
        System.out.println(Void.TYPE == void.class);
        System.out.println(Long.TYPE == long.class);
        System.out.println(long.class);
        A a = new B();
        a.f();
        if (a instanceof B) {
            System.out.println("instanceof B!");
            ((B) a).g();
        }
    }
}

interface A {
    void f();
}

class B implements A {

    @Override
    public void f() {
        System.out.println("f");
    }

    public void g() {
        System.out.println("g");
    }


}
