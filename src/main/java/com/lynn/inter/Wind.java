package com.lynn.inter;

public class Wind extends Instrument {
    void play() {
        System.out.println("Play");
    }

    @Override
    String what() {
        return "Wind";
    }

    void adjust() {
        System.out.println("Adjust");
    }
}
