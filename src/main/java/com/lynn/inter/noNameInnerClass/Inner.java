package com.lynn.inter.noNameInnerClass;

import com.lynn.inter.outer.Contents;

public class Inner {
    private int i = 0;
    public Contents contents() {
        return new Contents() {
            public int value() {
                while (i < 10)
                    i++;
                return i;
            }
        };
    }

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }

    public static void main(String[] args) {
        Inner inner = new Inner();
        System.out.println(inner.contents().value());
    }
}
