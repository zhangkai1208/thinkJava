package com.lynn.inter;

abstract class Instrument {
    abstract void play();

    String what() {
        return "Instrument";
    }

    abstract void adjust();
}
