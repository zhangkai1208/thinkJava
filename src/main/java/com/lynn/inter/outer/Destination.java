package com.lynn.inter.outer;

public interface Destination {
    String readLabel();
}
