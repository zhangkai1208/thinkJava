package com.lynn.inter.outer;

public class Outer {
    class Inner {
        void doInner() {
            System.out.println("doInner");
        }
    }

    Outer getOuter() {
        return Outer.this;
    }

    public static void main(String[] args) {
//        Outer.Inner innerObj = new Outer().new Inner();
//        innerObj.doInner();
//        Outer outer = new Outer();
//        Outer outer1 = outer.getOuter();
//        outer1.new Inner();
        Parcel parcel = new Parcel();
        Contents contents = parcel.contents();
        Destination destination = parcel.destination("Tasmania");
//        Parcel.PContents pContents = parcel.new PContents(); don't have access! because is private class!
    }
}
