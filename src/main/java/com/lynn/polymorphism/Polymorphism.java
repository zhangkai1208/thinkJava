package com.lynn.polymorphism;

public class Polymorphism {
    public static void main(String[] args) {
        Mill mill = new Mill();
        System.out.println(mill);
        Grain g = mill.process();
        System.out.println(g);
        mill = new WheatMill();
        System.out.println(mill);
        g = mill.process();
        System.out.println(g);
    }
}
class Grain {
    @Override
    public String toString() {
        return "Grain";
    }
}

class Wheat extends Grain {
    @Override
    public String toString() {
        return "Wheat";
    }
}

class Mill {
    Grain process() {
        return new Grain();
    }
}

class WheatMill extends Mill {
    Wheat process() {
        return new Wheat();
    }
}
