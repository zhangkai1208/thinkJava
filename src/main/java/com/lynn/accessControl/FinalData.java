package com.lynn.AccessControl;

import java.util.Random;

public class FinalData {
    private static Random rand = new Random(47);
    private String id;

    public FinalData(String id) {
        this.id = id;
    }

    private final int valueOne = 9;
    private static final int VALUE_TWO = 99;
    private static final int VALUE_THREE = 39;
    private static final int i4 = rand.nextInt(20);
    static final int INT_5 = rand.nextInt(20);
    private Value v1 = new Value(11);
    private final static Value v2 = new Value(22);
    private static final Value VALUE_3 = new Value(33);

    private final int[] a = {1, 2, 3, 4, 5, 6};
    private Value value = null;

    public static void main(String[] args) {
        FinalData finalData = new FinalData("f1");
        finalData.v2.i++;
        System.out.println(finalData.v2.i);
        if (finalData.value == null) {
            finalData.value = new Value(121);
        }
    }

}

class Value {
    int i;

    public Value(int i) {
        this.i = i;
    }
}

class Value2 {
    int i;

    public Value2(int i) {
        this.i = i;
    }

    public final void setI(int i) {
        this.i = i;
    }

    public final int getI() {
        return i;
    }

    @Override
    public String toString() {
        return "Value2{" +
                "i=" + i +
                '}';
    }

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            Value2 value2 = new Value2(i);
            System.out.println(value2.getI());
            System.out.println(value2);
        }
    }
}
