package com.lynn.AccessControl;

import com.lynn.AccessControl.Subdirectory.Cookie;

public class ChocolateChip extends Cookie {
    public ChocolateChip() {
        System.out.println(this.getClass().getSimpleName() + "      constructor!");
    }

    public void chomp() {
        bite();
        System.out.println(this.getClass().getSimpleName() + "        chomp()");
    }
}
