package com.lynn.contflow;

import org.junit.jupiter.api.Test;

import java.util.Random;

public class Control {
    /**
     * 带标签的控制流
     * 指定一个key,value为一个循环 通过breake key可跳出key的循环 而通过continue key可以继续开始该循环
     */
    @Test
    public void testContiAndBreake() {
        int i = 0;
        outer:
        for (; true; ) {
            inner:
            for (; i < 10; i++) {
                System.out.print("i = " + i);
                if (i == 2) {
                    System.out.print("continue");
                    continue;
                }
                if (i == 3) {
                    System.out.print("break");
                    i++;
                    break;
                }
                if (i == 7) {
                    System.out.print("continue outer");
                    i++;
                    continue outer;
                }
                if (i == 8) {
                    System.out.print("break outer");
                    break outer;
                }
                for (int j = 0; j < 6; j++) {
                    if (j == 3) {
                        System.out.print("continue inner");
                        continue inner;
                    }
                }
            }
        }
    }

    @Test
    public void testWhile() {
        int i = 0;
        outer:
        while (true) {
            System.out.println("outer loop!");
            inner:
            while (true) {
                i++;
                System.out.println("inner loop!");
                if (i == 1) {
                    System.out.print("continue");
                    continue;
                }
                if (i == 3) {
                    System.out.print("continue outer");
                    continue outer;
                }
                if (i == 5) {
                    System.out.print("break");
                    break;
                }
                if (i == 7) {
                    System.out.print("continue outer");
                    break outer;
                }
            }
        }
    }
    @Test
    public void testRandom() {
        Random random = new Random();
        int rInt = random.nextInt(26);
        int res = rInt + 'a';
        System.out.println(rInt);
        System.out.println((int) "a".toCharArray()[0] );
        System.out.println(res);
    }

}
