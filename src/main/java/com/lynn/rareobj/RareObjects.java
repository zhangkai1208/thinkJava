package com.lynn.rareobj;

import org.junit.jupiter.api.Test;

import java.util.*;

public class RareObjects {
    @Test
    public void testStack() {
        Stack<String> stack = new Stack<String>();
        for (String i : "Just have fun!".split(" ")) {
            stack.push(i);
        }
        while (!stack.isEmpty()) {
            System.out.println(stack.pop() + " " + stack.size());
        }
    }

    @Test
    public void testHashSet() {
        /**
         * HashSet的迭代器在输出时“不保证有序”，但也不是“保证无序”。也就是说，输出时有序也是允许的，但是你的程序不应该依赖这一点。
         * 白话文就是通过hashCode进行了排序处理,举个例子
         *
         * */
        Set<Integer> set = new HashSet<Integer>();
        Random random = new Random(47);
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
        for (Integer i = 0; i < 1000; i++) {
            map.put(i, i.hashCode());
            set.add(random.nextInt(30));
        }
        System.out.println(set);
        System.out.println(map);
        System.out.println("-------------分界线------------------");
        Random rand = new Random(47);
        Set<Integer> intset = new HashSet<Integer>();
        for (int i = 0; i < 10000; i++) {
            intset.add(rand.nextInt(30) + (1 << 16));
        }
        Iterator<Integer> iterator = intset.iterator();
        while (iterator.hasNext()) {
            System.out.print((iterator.next() - (1 << 16)) + " ");
        }
        System.out.println("-------------分界线------------------");
        Set<Integer> setTree = new TreeSet<Integer>();
        for (int i = 0; i < 1000; i++) {
            setTree.add(random.nextInt(30));
        }
        System.out.println(setTree);
    }

    private static void printQ(Queue queue) {
        while (queue.peek() != null) {
            System.out.print(queue.remove() + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        Queue<Integer> queue = new LinkedList<Integer>();
        Random random = new Random(47);
        for (int i = 0; i < 10; i++) {
            queue.offer(random.nextInt(i + 10));
        }
        printQ(queue);

        Queue<Character> characterQueue = new LinkedList<Character>();
        for (char c : "characterQueue".toCharArray()) {
            characterQueue.offer(c);
        }
        printQ(characterQueue);
    }


}
