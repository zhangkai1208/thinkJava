package com.lynn.genericity;

import com.sun.org.apache.xpath.internal.operations.String;

import javax.xml.ws.Holder;
import java.util.LinkedList;
import java.util.List;

/**
 * 古老的泛型
 */
public class Genericy {
    //    public static void main(String[] args) {
//        Holder<Automobile> hodler = new Holder<Automobile>();
//    }
    public static void main(String[] args) {
        List<Integer> list = Automobile.list();
        list.add(1);
        System.out.println(list);
    }
}

class Automobile {

    public static <T> List<T> list() {
        return new LinkedList<>();
    }


}

class Hodler<T> {
    private T automobile;

    public Hodler(T automobile) {
        this.automobile = automobile;
    }

    T get() {
        return automobile;
    }
}
