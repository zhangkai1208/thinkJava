package com.lynn.haveObject;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class Iter {
    public static void main(String[] args) {
        List<Integer> integerList = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);
        test(integerList);

    }

    public static void test(Iterable it) {
        for (Object o : it) {
            System.out.println(o);
        }
    }
}
