package com.lynn.haveObject;

import java.util.*;

public class ListIter {
    static List fill() {
        List<String> strings = Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8", "9");
        return strings;
    }

    public static void main(String[] args) {
        ListIterator listIterator = fill().listIterator();
        while (listIterator.hasNext()) {
            System.out.println(listIterator.next() + "," + listIterator.nextIndex() + "," + listIterator.previousIndex());
        }
    }
}
