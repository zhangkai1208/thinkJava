package jdk18.function18.function;

import java.util.function.Function;

class FunctionTest{
    public static void main(String[] args) {
        Function<Double, Double> sqrt = input -> Math.sqrt(input);
        System.out.println(sqrt.apply(4.0));
    }
}
