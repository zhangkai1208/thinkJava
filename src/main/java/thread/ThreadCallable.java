package thread;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

public class ThreadCallable {

    public static void main(String[] args) throws Exception {
        ThreadDemo threadDemo = new ThreadDemo();
        FutureTask<Integer> futureTask = new FutureTask<Integer>(threadDemo);
        new Thread(futureTask).start();//FauterTask可用于闭锁
        Integer integer = futureTask.get();
        System.out.println(integer);
    }
}

class ThreadDemo implements Callable<Integer> {

    @Override
    public Integer call() throws Exception {
        int sum = 0;
        for (int i = 0; i <= 100; i++) {
            System.out.println(i);
            sum += i;
        }
        return sum;
    }
}
