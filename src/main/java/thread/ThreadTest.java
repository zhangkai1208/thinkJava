package thread;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import jdk.nashorn.internal.objects.annotations.Constructor;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.Date;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static thread.ReadFileThread.readFileSync;

public class ThreadTest {
    public static void main(String[] args) throws InterruptedException {
        //一个线程去读取两个文件
        /**单个线程读取多个文件
         * */
//        ReadFileThread readFileThread = new ReadFileThread("src/main/resources/package.json", "src/main/resources/package2.json");
//        new Thread(readFileThread).start();

        /**多个线程读取一个文件* */
        CountDownLatch countDownLatch = new CountDownLatch(2);
//        ReadFileThreadMore readFileThreadMore = new ReadFileThreadMore(countDownLatch, "src/main/resources/package.json");
//        long start = System.currentTimeMillis();
//        for (int i = 0; i < 2; i++) {
//            new Thread(readFileThreadMore).start();
//        }
        try {
            countDownLatch.await();//注掉看效果
            long stop = System.currentTimeMillis();
//            System.out.println("程序结束了，总耗时：" + String.valueOf(stop - start) + " ms(毫秒)！\n");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}

@Data
@AllArgsConstructor
class ReadFileThreadMore implements Runnable {

    private CountDownLatch latch;
    private String fileName;

    public ReadFileThreadMore() {
    }

    public ReadFileThreadMore(CountDownLatch latch) {
        this.latch = latch;
    }

    @Override
    public void run() {
        try {
            System.out.println("读取文件" + fileName + "完成:" + readFileSync(fileName));
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            latch.countDown();//减1
        }
    }
}


@Data
@AllArgsConstructor
class ReadFileThread implements Runnable {

    private String fileName;
    private String fileName2;

    @Override
    public void run() {
        try {
            long start = System.currentTimeMillis();
            System.out.println("读取文件" + fileName + "完成:" + readFileSync(fileName));
            if (null != fileName2) {
                System.out.println("读取文件" + fileName2 + "完成:" + readFileSync(fileName2));
            }
            long stop = System.currentTimeMillis();
            System.out.println("程序结束了，总耗时：" + String.valueOf(stop - start) + " ms(毫秒)！\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static JSONObject readFileSync(String fileName) throws IOException {
        File file = new File(fileName);
        InputStreamReader inputStreamReader = new InputStreamReader(new FileInputStream(file));
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        StringBuilder sb = new StringBuilder("{");
        while (null != bufferedReader.readLine()) {
            sb.append(bufferedReader.readLine());
        }
        sb.append("}");
        return JSON.parseObject(sb.toString());
    }
}
