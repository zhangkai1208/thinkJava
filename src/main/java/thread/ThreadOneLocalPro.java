package thread;

import lombok.Data;
import org.junit.jupiter.api.Test;

public class ThreadOneLocalPro {

    private static Man man = new Man();
    private static final ThreadLocal<Man> THREAD_LOCAL = new ThreadLocal<>();

    public void set() {
        THREAD_LOCAL.set(man);
    }

    @Test
    public void testThreadLocal() {
        final Thread[] threads = new Thread[5];
        for (int i = 0; i < 5; i++) {
            threads[i] = new Thread(new Runnable() {
                @Override
                public void run() {
                    set();//初始化或者是initialValue 赋值否则空指针异常
                    THREAD_LOCAL.get().setName(THREAD_LOCAL.get().getName() + "+");
                    System.out.println(Thread.currentThread().getName() + ":" + THREAD_LOCAL.get().getName());
                }
            });
        }
        for (Thread thread : threads) {
            thread.start();
        }
    }
//            new ThreadLocal<Man>() {
//        @Override
//        protected Man initialValue() {
//            return man;
//        }
//    };
//}
}

class Man {
    String name = "NoName";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
