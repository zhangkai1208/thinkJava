package thread.juc.cas;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 会有多个线程操作同一数据，破坏了原子性
 * 解决办法：jdk1.5 Java.util.cocyrrent.atomic提供了常用的原子变量
 * 1. volatile 保证内存可见性
 * 2  cas(Compare-and-swap) 算法保证数据原子性 : 是对硬件对并发共享数据的支持
 * cas包含了三个操作数
 * 内存值  V
 * 预估值  A（旧值）
 * 更新值  B
 * 当且仅当 V == A 时， V 赋值 B 否则什么都不做!
 */
public class AtomicDemo {
    //产生重复数据，原子性问题
    public static void main(String[] args) {
        AtomicThread atomicThread = new AtomicThread();
        for (int i = 0; i < 10; i++) {
            new Thread(atomicThread).start();
        }
    }
}

class AtomicThread implements Runnable {
    //volatile也不能保证共享数据的原子性
//    private int serialNunmber = 0;
    private AtomicInteger serialNnmber = new AtomicInteger();

    @Override
    public void run() {
        try {
            Thread.sleep(200);
            System.out.println(Thread.currentThread().getName() + ":" + getSerialNunmber());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public int getSerialNunmber() {
        return serialNnmber.getAndIncrement();
    }
}
