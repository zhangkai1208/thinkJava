package thread.juc.volatilePro;

import lombok.Data;
/**volatie 不具有互斥性，不存在抢锁之说! 多个线程可以同时访问
 *          不能保证变量的原子性，
 * */
public class VolatieTest {
    public static void main(String[] args) {
        ThreadOne threadOne = new ThreadOne();
        new java.lang.Thread(threadOne).start();
        while (true) {
            //①线程同步,刷新主内存中的数据，但是效率低
//            synchronized (threadOne) {
            if (threadOne.isFlag()) {
                System.out.println("--------------------------");
                break;
//                }
            }
        }
    }
}

class ThreadOne implements Runnable {
    private volatile boolean flag;
    //②多线程内共享变量/数据是可见的（实时刷新缓存中的数据）
    //    private boolean flag;

    @Override
    public void run() {
        try {
            java.lang.Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        flag = true;
        System.out.println("flag value is [" + isFlag() + "]");
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }
}
