package thread.juc.pool;

import java.util.concurrent.*;

public class TestPool {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService executor = Executors.newFixedThreadPool(5);//new ScheduledThreadPoolExecutor(100);

        Future<Integer> sumInt = executor.submit(() -> {
            int sum = 0;
            for (int i = 0; i <= 100; i++) {
                sum += i;
            }
            return sum;
        });

        System.out.println(sumInt.get());

        //为线程池中的线程分配任务
//        executor.submit(() -> {
//            for (int i = 1; i <= 100; i++) {
//                System.out.println(Thread.currentThread().getName() + " : " + i);
//            }
//        });

        executor.shutdown();//延迟关闭

    }
}

class PoolDemo {


    public void get() {

    }
}
