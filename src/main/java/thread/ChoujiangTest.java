package thread;

//1.有一个抽奖池,该抽奖池中存放了奖励的金额,该抽奖池用一个数组int[] arr = {10,5,20,50,100,200,500,800,2,80,300};
//创建两个抽奖箱(线程)设置线程名称分别为“抽奖箱1”，“抽奖箱2”，随机从arr数组中获取奖项元素并打印在控制台上,格式如下:
public class ChoujiangTest {
    public static void main(String[] args) {
        new Thread(new Chou(), "抽奖箱1").start();
        new Thread(new Chou(), "抽奖箱2").start();
    }
}

class Chou implements Runnable {
    int arr[] = {10, 5, 20, 50, 100, 200, 500, 800, 2, 80, 300};
    int num = arr.length;
    boolean[] flag = new boolean[num];

    @Override
    public void run() {
        while (true) {
            //保证并发
            synchronized (this) {
                if (num > 0) {
                    int index = (int) (Math.random() * arr.length);
                    int get = arr[index];
                    // 代表这张抽奖券抽过了
                    if (!flag[index]) {
                        flag[index] = true;
                        System.out.println(Thread.currentThread().getName() + " 又产生了一个" + get + "元大奖");
                        num--;
                    }
                }
            }
        }

    }
}
