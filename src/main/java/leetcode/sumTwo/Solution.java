package leetcode.sumTwo;

import java.util.Arrays;

/**
 * @Description:
 * @Date: 2019/3/2 11:47
 * @Auther: lynn
 */
public class Solution {
    public int[] TwoSum(int[] nums, int target) {
        int[] b = new int[2];
        for (int i = 0; i <= nums.length - 1; i++) {
            int a = target - nums[i];

            for (int j = i + 1; j <= nums.length - 1; j++) {
                if (a == nums[j]) {
                    b[0] = i;
                    b[1] = j;
                }
            }
        }
        return b;
    }

    public int[] TwoSumYouSelf(int[] nums, int target) {
        int[] b = new int[2];
        for (int i = 0; i <= nums.length - 2; i++) {
            int a = target - nums[i];
            for (int j = i + 1; j <= nums.length - 1; j++) {
                if (a == nums[j]) {
                    b[0] = i;
                    b[1] = j;
                }
            }
        }
        return b;
    }

    public static void main(String[] args) {
        int[] nums = {1, 3, 4, 5};
        System.out.println(Arrays.toString(new Solution().TwoSum(nums, 9)));
    }
}
