package leetcode.ListTotal;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

//
public class ListAndArray {
    @Test
    public void testThreeSum() {
        int[] nums = {-1, 0, 1, 2, -1, -4, 4, 7, 0};//{-1,0,1},{-1,2,-1}}
        System.out.println(threeSum(nums));
    }

    /**
     * 例如, 给定数组 nums = [-1, 0, 1, 2, -1, -4]，
     * 满足要求的三元组集合为：[[-1, 0, 1],[-1, -1, 2]]
     */
    public List<List<Integer>> threeSum(int[] nums) {
        List<List<Integer>> listTotal = new ArrayList<List<Integer>>();
        List<Integer> list = null;
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                for (int k = j + 1; k < nums.length; k++) {
                    if ((nums[i] + nums[j] + nums[k]) == 0) {
                        list = new ArrayList<>();
                        list.add(nums[i]);
                        list.add(nums[j]);
                        list.add(nums[k]);
                        list.sort(new Comparator<Integer>() {
                            @Override
                            public int compare(Integer o1, Integer o2) {
                                return o1.compareTo(o2);
                            }
                        });
                        if (!listTotal.contains(list)) {
                            listTotal.add(list);
                        }
                    }
                }
            }
        }
        return listTotal;
    }
}
