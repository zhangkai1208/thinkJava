package leetcode.array;

/**
 * 给定一个整数类型的数组 nums，请编写一个能够返回数组“中心索引”的方法。
 * 我们是这样定义数组中心索引的：数组中心索引的左侧所有元素相加的和等于右侧所有元素相加的和。
 * 如果数组不存在中心索引，那么我们应该返回 -1。如果数组有多个中心索引，那么我们应该返回最靠近左边的那一个。
 */
public class ArrayAri {
    public static int pivotIndex(int[] nums) {
        for (int i = 0; i < nums.length; i++) {
            int a = 0;
            int b = 0;
            for (int j = 0; j < i; j++) {
                a += nums[j];
            }
            for (int t = i + 1; t < nums.length; t++) {
                b += nums[t];
            }
            if (a == b) {
                return i;
            }
        }
        return 0;
    }

    public static void main(String[] args) {
        int[] nums = {1, 1, -1, 1, 3, 1, 1, -1, 1};//test1
//        int[] nums = {1, 7, 3, 6, 5, 6};
        System.out.println(pivotIndex(nums));
    }
}
