package leetcode.array;

/**
 * 在一个给定的数组nums中，总是存在一个最大元素 。
 * 查找数组中的最大元素是否至少是数组中每个其他数字的两倍。
 * 如果是，则返回最大元素的索引，否则返回-1。
 */
public class ArrayMax {
    public static void main(String[] args) {
        int[] nums = {0, 0, 4, 0, 1, 9, 3};
//        int[] nums = {1, 2, 3, 4};
        System.out.println(new Solution().dominantIndex(nums));
    }
}

class Solution {
    public int dominantIndex(int[] nums) {
        int index = 0;
        int max = nums[0];
        for (int i = 0; i < nums.length - 1; i++) {
            if (max > nums[i + 1]) {
                index = i;
            } else {
                max = nums[i + 1];
                index = i + 1;
            }
        }
        int j = 0;
        while (j < nums.length) {
            if (index == j) {
                break;
            }
            if (max < 2 * nums[j]) {
                System.out.println(j);
                return -1;
            }
            j++;
        }
        return index;
    }
}
