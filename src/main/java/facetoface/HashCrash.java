package facetoface;

import java.util.HashMap;

public class HashCrash {
    public static void main(String[] args) {
        HashMap<App, String> hash = new HashMap();
        Long start = System.currentTimeMillis();
        System.out.println("-----one  begin-----");
        for (Integer i = 0; i < 1231; i++) {
            App app = new App();
            app.setId(100);
            hash.put(app, i.toString());
        }
        App app2 = new App();
        app2.setId(100);
        hash.get(app2);
        Long end = System.currentTimeMillis();
        System.out.println("第一种方式" + (end - start) / 1000);//33
        System.out.println("-----two begin-----");
        HashMap<App, String> hash2 = new HashMap<>();
        Long start2 = System.currentTimeMillis();
        for (Integer i = 0; i < 65536; i++) {
            App app = new App();
            app.setId(i);
            hash2.put(app, i.toString());
        }
        App app3 = new App();
        app3.setId(100);
        hash2.get(app3);
        Long end2 = System.currentTimeMillis();
        System.out.println("第2种方式" + (end2 - start2) / 1000);
    }
}

class App {
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object obj) {
        // TODO Auto-generated method stub
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        // TODO Auto-generated method stub
        return id;
    }
}
