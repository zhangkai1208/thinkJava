package com.lynn.spring;

import com.lynn.spring.service.HelloService;
import com.lynn.spring.service.config.SpringConfig;
import com.lynn.spring.service.impl.HelloWorldService;
import com.lynn.spring.service.impl.StrutsHelloWorld;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @Description:
 * @Date: 2019/1/14 16:40
 * @Auther: lynn
 */
public class Application {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        HelloWorldService service = (HelloWorldService) context.getBean("helloWorldService");
        HelloService hw = service.getHelloService();
        hw.sayHello();
        //如果使用类配置,则需要这种
        context = new AnnotationConfigApplicationContext(SpringConfig.class);
        StrutsHelloWorld strutsHelloWorld = (StrutsHelloWorld) context.getBean("strutsHelloWorld");
        strutsHelloWorld.sayHello();
    }
}
