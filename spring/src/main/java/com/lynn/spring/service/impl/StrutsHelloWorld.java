package com.lynn.spring.service.impl;

import com.lynn.spring.service.HelloService;

/**
 * @Description:
 * @Date: 2019/1/14 16:45
 * @Auther: lynn
 */

public class StrutsHelloWorld implements HelloService {
    @Override
    public void sayHello() {
        System.out.println("struts say hello!");
    }
}
