package com.lynn.spring.service.impl;

import com.lynn.spring.service.HelloService;

/**
 * @Description:
 * @Date: 2019/1/14 16:43
 * @Auther: lynn
 */
public class SpringHelloWorld implements HelloService {
    @Override
    public void sayHello() {
        System.out.println("say hello!");
    }
}
