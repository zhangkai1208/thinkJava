package com.lynn.spring.service.config;

import com.lynn.spring.service.HelloService;
import com.lynn.spring.service.impl.StrutsHelloWorld;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description:
 * @Date: 2019/1/14 17:10
 * @Auther: lynn
 */
@Configuration
public class SpringConfig {

    @Bean(name = "strutsHelloWorld")
    public HelloService helloWorld() {
        return new StrutsHelloWorld();
    }

}
