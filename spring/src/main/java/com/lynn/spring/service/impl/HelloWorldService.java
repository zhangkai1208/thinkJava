package com.lynn.spring.service.impl;

import com.lynn.spring.service.HelloService;

/**
 * @Description:
 * @Date: 2019/1/14 16:41
 * @Auther: lynn
 */
public class HelloWorldService {
    HelloService helloService;

    public HelloWorldService() {
    }

    public HelloService getHelloService() {
        return helloService;
    }

    public void setHelloService(HelloService helloService) {
        this.helloService = helloService;
    }

}
