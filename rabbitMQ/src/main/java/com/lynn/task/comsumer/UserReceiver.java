package com.lynn.task.comsumer;

import com.lynn.cloudweb.service.UserService;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Description:
 * @Date: 2019/2/22 16:42
 * @Auther: lynn
 */
@Component
@RabbitListener(queues = "register")
public class UserReceiver {

    @Autowired
    UserService userService;

    @RabbitHandler
    public void process(String hello) {
        System.out.println("接收到消息队列中的值  : " + hello + "正在处理");
        //处理用户发邮件操作
        userService.notifyUser();
        System.out.println("处理用户发邮件操作执行成功了!");
    }
}
