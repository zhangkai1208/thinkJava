package com.lynn.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Description:
 * @Date: 2019/2/22 16:23
 * @Auther: lynn
 */
@Component
public class RabbitmqUtils {
    private static final Logger logger = LoggerFactory.getLogger(RabbitmqUtils.class);

    @Autowired
    RabbitTemplate rabbitTemplate;


    //消息的产生方，负责将消息推送到消息队列
    public void publish(String queueName, Object obj) {
        rabbitTemplate.convertAndSend(queueName, obj);
    }
}
