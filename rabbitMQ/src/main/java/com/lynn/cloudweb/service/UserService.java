package com.lynn.cloudweb.service;

/**
 * @Description:
 * @Date: 2019/2/22 09:04
 * @Auther: lynn
 */
public interface UserService {
    String notifyUser();
    String register();
}
