package com.lynn.cloudweb.service.impl;

import com.lynn.cloudweb.service.UserService;
import com.lynn.task.RabbitmqUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description:
 * @Date: 2019/2/22 09:04
 * @Auther: lynn
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    RabbitmqUtils rabbitmqUtils;

    @Override
    public String notifyUser() {
        //这里调用订阅消息实现推送消息或者各类邮件给用户
        return null;
    }

    @Override
    public String register() {
        System.out.println("用户注册成功了!,正在跳转登陆页面");

        //将发邮件和短信通知发布到消息队列中
        rabbitmqUtils.publish("register", "注册和发送邮件队列!");
        System.out.println("队列注册成功");
        return "用户注册成功了!";
    }
}
