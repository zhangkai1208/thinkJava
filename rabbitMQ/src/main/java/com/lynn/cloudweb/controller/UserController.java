package com.lynn.cloudweb.controller;

import com.lynn.cloudweb.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @Date: 2019/2/22 09:03
 * @Auther: lynn
 */
@Api(tags = "用户模块: 用户管理")
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    //用户处理逻辑接口
    public String notifyUser() {
        return userService.notifyUser();
    }

    //用户注册
    @ApiOperation(value = "用户注册", notes = "用户注册")
    @PostMapping("/user/register")
    public String register() {
        return userService.register();
    }

}
