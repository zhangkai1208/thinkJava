package com.lynn.entities;

import lombok.Data;
import lombok.ToString;

/**
 * @Description:
 * @Date: 2019/3/6 08:52
 * @Auther: lynn
 */
@Data
@ToString
public class User {
    private String username;
    private String password;
    private String age;
    private String realName;
}
