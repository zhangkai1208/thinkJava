package com.lynn.controller;

import com.lynn.entities.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


/**
 * @Description: Functional Programming Model
 * @Date: 2019/3/5 09:04
 * @Auther: lynn
 */
@RestController
public class UserController {

    Logger logger = LoggerFactory.getLogger("UserController");

    @GetMapping("/hello")
    public Mono<String> hellowebflux() {
        //这是如何将请求主体提取到Mono<String>：
//        Mono<String> str = request.bodyToMono(String.class);
//        logger.debug(str.block());
//        封装实体对象
//        Flux<User> userFlux = request.bodyToFlux(User.class);
//        logger.debug(userFlux.blockFirst().toString());

        System.out.println("hello webflux");
        return Mono.just("Welcome to reactive world ~");
    }

}
